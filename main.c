/*
 * File:   main.c
 * Author: aogochi
 *
 * Created on 2019/08/26, 13:43
 */

//I.O port
//RA0 ICSPADAT		:RA1 ICSPCLK	:RA2 NC			:RA3 MCLR		:RA4 NC			:RA5 NC
//RC0(AN4) VolumeIN	:RC1 OPA1IN-	:RC2 OPA1OUT	:RC3 OPA2OUT	:RC4 OPA2IN-	:RC5 NC

#include "mcc_generated_files\mcc.h"

#define drive_mA 150                                    //電流設定 0～200mA @FVR 2.048v
#define drive_set drive_mA/0.8                          // 0.8mA/bit  @FVR 2.048v
#define LED_current 10                                  //FET非駆動時のLED点灯電流 8uA/bit @FVR 2.048v

uint8_t duty = 5;                                       //動作デューティ
uint8_t timer_count = 0;

const uint8_t table[8] = {60, 45, 30, 20, 15, 10, 5, 2};    //デューティ設定テーブル


void main(void) {
    uint8_t ADvalue;
    SYSTEM_Initialize();

    DAC1CON1 = drive_set;

    while(1){
        if(TMR6IF != 0)                     //タイマ6割り込みフラグ待ち
        {
            TMR6IF = 0;
    
            if(++timer_count < duty)
            {
                //ダミー負荷OFFの処理
                DAC1CON1 = LED_current;     //設定電流変更
                OPA1EN = 0;                 //FET駆動オペアンプ無効
            }
            else
            {
                //ダミー負荷ONの処理
                DAC1CON1 = drive_set;   //設定電流変更
                OPA1EN = 1;                 //FET駆動オペアンプ有効
                timer_count = 0;            //パルス駆動用カウンタリセット
            }

            //可変抵抗の電圧を測定
            ADC_SelectChannel(4);
            ADC_StartConversion();
            while(ADCON0bits.GO_nDONE);
            ADvalue = ADRESH;               //AD値の上位8bitを取得
            ADvalue = ADvalue >> 5;

            //可変抵抗の値でFET駆動デューティを変更
            duty = table[ADvalue];
        }
   }
}
